﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Web.UI;

namespace ASEA1
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
           
            this.DoubleBuffered = true;

        }
        //code to convert HSB to RGB from HSB.cs 
        public struct HSBColor
        {
            float h;
            float s;
            float b;
            int a;

            public HSBColor(float h, float s, float b)
            {
                this.a = 0xff;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public HSBColor(int a, float h, float s, float b)
            {
                this.a = a;
                this.h = Math.Min(Math.Max(h, 0), 255);
                this.s = Math.Min(Math.Max(s, 0), 255);
                this.b = Math.Min(Math.Max(b, 0), 255);
            }

            public float H
            {
                get
                {
                    return h;
                }
            }

            public float S
            {
                get
                {
                    return s;
                }
            }

            public float B
            {
                get
                {
                    return b;
                }
            }

            public int A
            {
                get
                {
                    return a;
                }
            }

            public Color Color
            {
                get
                {
                    return FromHSB(this);
                }
            }

            public static Color FromHSB(HSBColor hsbColor)
            {
                float r = hsbColor.b;
                float g = hsbColor.b;
                float b = hsbColor.b;
                if (hsbColor.s != 0)
                {
                    float max = hsbColor.b;
                    float dif = hsbColor.b * hsbColor.s / 255f;
                    float min = hsbColor.b - dif;
                    float h = hsbColor.h * 360f / 255f;
                    if (h < 60f)
                    {
                        r = max;
                        g = h * dif / 60f + min;
                        b = min;
                    }
                    else if (h < 120f)
                    {
                        r = -(h - 120f) * dif / 60f + min;
                        g = max;
                        b = min;
                    }
                    else if (h < 180f)
                    {
                        r = min;
                        g = max;
                        b = (h - 120f) * dif / 60f + min;
                    }
                    else if (h < 240f)
                    {
                        r = min;
                        g = -(h - 240f) * dif / 60f + min;
                        b = max;
                    }
                    else if (h < 300f)
                    {
                        r = (h - 240f) * dif / 60f + min;
                        g = min;
                        b = max;
                    }
                    else if (h <= 360f)
                    {
                        r = max;
                        g = min;
                        b = -(h - 360f) * dif / 60 + min;
                    }
                    else
                    {
                        r = 0;
                        g = 0;
                        b = 0;
                    }
                }
                return Color.FromArgb
                    (
                        hsbColor.a,
                        (int)Math.Round(Math.Min(Math.Max(r, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(g, 0), 255)),
                        (int)Math.Round(Math.Min(Math.Max(b, 0), 255))
                    );
            }
        }

        private const int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private static bool finished;
        private static float xy;
        private Cursor c1, c2;
        Rectangle rec;
        int c = 0;

        private Graphics g1;

        private System.Drawing.Bitmap bitmap;


        public void init()
        {
            //djm setSize(640, 480); Changed to line below in c#
            this.Size = new Size(640, 480);
            finished = false;
            // djm JAVA x1 = getSize().width;
            //djm JAVA y1 = getSize().height;
            x1 = 640;
            y1 = 480;
            xy = (float)x1 / (float)y1;

            // djm JAVA picture = createImage(x1, y1);
            bitmap = new Bitmap(x1, y1);
            // djm JAVA g1 = picture.getGraphics();
            g1 = Graphics.FromImage(bitmap);
            finished = true;

        }
        public void destroy()
        {
            if (finished)
            {
                // djm JAVA picture = null;
                bitmap = null;
                g1 = null;
                // djm JAVA System.gc(); 
                GC.Collect(); // Garabage Collection 
            }
        }
        
        // All same just deleted JAVA statements
        public void start()
        {
            // djm JAVA action = false; NOT NEEDED
            // djm JAVA rectangle = false; NOT NEEDED
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }
        public void stop()
        {
        }
        
        /*public void paint(Graphics g, PaintEventArgs e) djm JAVA
        {
            update(g);
        }
       public void update(Graphics g)
        {
            //g.DrawImage(picture, 0, 0);
          ALL NOT NEEDED! 
        }*/
        private void mandelbrot()
        {
            int x, y;
            float h, b, alt = 0.0f;
            /*action = false;
            setCursor(c1);      djm JAVA NOT NEEDED! 
            showStatus("Mandelbrot-Set will be produced - please wait...");*/
            Color color;
            Pen pen = new Pen(Color.Black);
            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y, c);
                    if (h != alt)
                    {


                        b = 1.0f - h * h;

                        color = HSBColor.FromHSB(new HSBColor(h * 255, 0.8f * 255, b * 255));

                        pen = new Pen(color);

                        alt = h;

                    }
                    g1.DrawLine(pen, x, y, x + 1, y);
                }


        }
        private float pointcolour(double xwert, double ywert, int j)
        {
            double r = 0.0, i = 0.0, m = 0.0;
            //int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }
        private void initvalues()
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }
        //Draws bitmap 
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g1 = e.Graphics;
            g1.DrawImage(bitmap, 0, 0, x1, y1);
            using (Pen pen = new Pen(Color.White, 2))
            {
                e.Graphics.DrawRectangle(pen, rec);
            }



        }
        //added load method
        private void Form1_Load(object sender, EventArgs e)//runs functions on load
        {
            init();
            start();

       }

        //Sets cursor for mouse movements
        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            //Waits for cursor, then changes to crosshair
            c1 = Cursors.WaitCursor;
            c2 = Cursors.Cross;
            if (e.Button == MouseButtons.Left)
            {
                //Declares x and y points for rectangle from users input. 
                xe = e.X;
                ye = e.Y;
                Invalidate();

                if (xs < xe)
                {
                    if (ys < ye) rec = new Rectangle(xs, ys, (xe - xs), (ye - ys));
                    else rec = new Rectangle(xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) rec = new Rectangle(xe, ys, (xs - xe), (ye - ys));
                    else rec = new Rectangle(xe, ye, (xs - xe), (ys - ye));
                }
            }

           


        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            // same as mouseReleased function in JAVA!
            int z, w;
            //Creates the rectangle and makes it disappear after unclick! 
            rec = new Rectangle(0, 0, 0, 0);
            //e.consume();

            //xe = e.X;
            //ye = e.Y;
            if (xs > xe)
            {
                z = xs;
                xs = xe;
                xe = z;
            }
            if (ys > ye)
            {
                z = ys;
                ys = ye;
                ye = z;
            }
            w = (xe - xs);
            z = (ye - ys);
            if ((w < 2) && (z < 2)) initvalues();
            else
            {
                if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                else xe = (int)((float)xs + (float)z * xy);
                xende = xstart + xzoom * (double)xe;
                yende = ystart + yzoom * (double)ye;
                xstart += xzoom * (double)xs;
                ystart += yzoom * (double)ys;
            }
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();

            this.Invalidate();
            //Repaint();
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            // same as mousePressed in JAVA removed e.consume and if statement as not needed!
            xs = e.X;
            ys = e.Y;
            
        }

       
        //Menu strip for Save, reset and colour functions
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {

            string path = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            //string folderf = @"\Fractals";
            string filename = @"\Fractals";
            string filetype = @".jpeg";
            int mone = 0;

            while (File.Exists(path + filename + mone.ToString() + filetype))
            {
                mone++;
            }

            bitmap.Save(path + filename + mone.ToString() + filetype);
            MessageBox.Show("Saved To Desktop");


        }
        //Saves current state of fractal to "txt" file
        private void savestateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string stringxzoom = xzoom.ToString();
            string stringyzoom = yzoom.ToString();
            string stringystart = ystart.ToString();
            string stringxstart = xstart.ToString();
            string[] lines = { stringxzoom, stringyzoom, stringxstart, stringystart };
            System.IO.File.WriteAllLines(@"C:\Users\mike\Desktop\Writelines.txt", lines);

            MessageBox.Show("Current state saved!");

        }
        // Loads previous state of fractal from "txt" file
        private void loadstateToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] lines = System.IO.File.ReadAllLines(@"C:\Users\mike\Desktop\Writelines.txt");
            xzoom = System.Convert.ToDouble(lines[0]);
            yzoom = System.Convert.ToDouble(lines[1]);
            xstart = System.Convert.ToDouble(lines[2]);
            ystart = System.Convert.ToDouble(lines[3]);
            Refresh();

        }
       //Red colour change - original colour
        private void redToolStripMenuItem_Click(object sender, EventArgs e){
            c = 0;
            mandelbrot();
            Refresh();
       }
        // Green colour change 
        private void greenToolStripMenuItem_Click(object sender, EventArgs e){
            c = 50;
            mandelbrot();
            Refresh();  
        }
        // Blue colour change 
        private void blueToolStripMenuItem_Click(object sender, EventArgs e) {
            c = 150;
            mandelbrot();
            Refresh();
        }


        //Colour cycling 
        private void cycleToolStripMenuItem_Click(object sender, EventArgs e) {
            for (c = 0; c < 200; c += 10)
            {
            mandelbrot();
            Refresh();
            }
        }
        //Reset colour 
        private void resetToolStripMenuItem_Click(object sender, EventArgs e) {
            c = 0;

            mandelbrot();
            Refresh();
        }
    }
}
