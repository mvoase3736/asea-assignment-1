﻿namespace ASEA1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greenToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blueToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cycleToolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetFractalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveinstanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadinstanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();

            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(531, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";

            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveToolStripMenuItem,
            this.redToolToolStripMenuItem,
            this.greenToolToolStripMenuItem,
            this.blueToolToolStripMenuItem,
            this.cycleToolToolStripMenuItem,
            this.saveinstanceToolStripMenuItem,
            this.loadinstanceToolStripMenuItem,
            this.resetFractalToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // redToolStripMenuItem
            // 
            this.redToolToolStripMenuItem.Name = "redToolToolStripMenuItem";
            this.redToolToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.redToolToolStripMenuItem.Text = "Red Tool";
            this.redToolToolStripMenuItem.Click += new System.EventHandler(this.redToolStripMenuItem_Click);
            // 
            // greenToolToolStripMenuItem
            // 
            this.greenToolToolStripMenuItem.Name = "greenToolToolStripMenuItem";
            this.greenToolToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.greenToolToolStripMenuItem.Text = "Green Tool";
            this.greenToolToolStripMenuItem.Click += new System.EventHandler(this.greenToolStripMenuItem_Click);
            // 
            // blueToolToolStripMenuItem
            // 
            this.blueToolToolStripMenuItem.Name = "blueToolToolStripMenuItem";
            this.blueToolToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.blueToolToolStripMenuItem.Text = "Blue Tool";
            this.blueToolToolStripMenuItem.Click += new System.EventHandler(this.blueToolStripMenuItem_Click);
            // 
            // cycleToolToolStripMenuItem
            // 
            this.cycleToolToolStripMenuItem.Name = "cycleToolToolStripMenuItem";
            this.cycleToolToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cycleToolToolStripMenuItem.Text = "Cycle Tool";
            this.cycleToolToolStripMenuItem.Click += new System.EventHandler(this.cycleToolStripMenuItem_Click);
            // 
            // resetFractalToolStripMenuItem
            // 
            this.resetFractalToolStripMenuItem.Name = "resetFractalToolStripMenuItem";
            this.resetFractalToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.resetFractalToolStripMenuItem.Text = "Reset Fractal";
            this.resetFractalToolStripMenuItem.Click += new System.EventHandler(this.resetToolStripMenuItem_Click);
            // 
            // saveinstanceToolStripMenuItem
            // 
            this.saveinstanceToolStripMenuItem.Name = "saveinstanceToolStripMenuItem";
            this.saveinstanceToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.saveinstanceToolStripMenuItem.Text = "Save state";
            this.saveinstanceToolStripMenuItem.Click += new System.EventHandler(this.savestateToolStripMenuItem_Click);
            //
            // loadinstanceToolStripMenuItem
            // 
            this.loadinstanceToolStripMenuItem.Name = "loadinstanceToolStripMenuItem";
            this.loadinstanceToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.loadinstanceToolStripMenuItem.Text = "Load state";
            this.loadinstanceToolStripMenuItem.Click += new System.EventHandler(this.loadstateToolStripMenuItem_Click);
            //
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 414);
            this.Controls.Add(this.menuStrip1);
            this.Cursor = System.Windows.Forms.Cursors.Cross;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Fractal";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseUp);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greenToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blueToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cycleToolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetFractalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveinstanceToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadinstanceToolStripMenuItem;

       
       
    }
}

